% imageClassify.m
% Blinded Image Classifier
% 
% Inputs: none
%
% This program allows for the classification of images in a GUI. The
% program takes images from user-specified directories and presents them
% for classification in a random order. 
%
% The list of categories you wish to use, and how you want to view your
% images, can be selected in the "user specified parameters" section.
%
% To classify images from a single data set, choose a directory containing
% the images you wish to analyze.
%
% To classify images from multiple data sets, put each set of images in its
% own directory, and then put all of those folders into a master directory.
% Choose this master directory when running the program- the program will
% present images from all of the data sets in a random order, and then
% group the results by the directory they came from.
%
% By default, image types accepted are: TIFF, JPEG, PNG
%
% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

function results = imageClassify

%% User specified parameters

% catList: list of image categories to classify
% category names cannot start with a number or contain spaces or special characters
catList = {'category1','category2','category3','category4','category5','category6'};

% extensions: list of image file types to accept
extensions = {'*.tiff','*.tif','*.jpg','*.png'};

% panels: choose how you want the program to display your images
unscaled = true; % show the image with no intensity scaling
autoscaled = false; % show the image with min/max intensity scaling

% === end of user-specified parameters ===

%% selecting images

% check for empty user-specified parameters
if isempty(catList)
    error(sprintf('You have not selected any classification categories. \nPlease add categories to catList before procceding.'));
elseif isempty(extensions)
    error(sprintf('You have not enabled any image file extensions. \nPlease add extensions before proceeding.'));
elseif ~unscaled && ~autoscaled
    error(sprintf('You have not selected a display mode. \nPlease choose at least one display mode before proceeding.'));
end

% check for improper category names
temp = struct();
for i = 1:length(catList)
    try 
        temp.(catList{i}) = 1;
    catch
        error('Category names must not start with a number or contain spaces or special characters.');
    end
end   

% Prompt to choose directory
workingDir=uigetdir();
% Exit if cancel button is pressed
if workingDir == 0
    error ('Classification cancelled.');
end
% Append trailing / on directory name if needed
if workingDir(end) ~= '/'
    workingDir = [workingDir,'/'];
end

% Get list of image files in directory
% In the cell array 'files', column 1: file name, column 2: directory
% index, column 3: classified category
% Directory index refers to which directory in subDir the image is found in
files = {};
for i = 1:size(extensions,2)
    temp = dir([workingDir,extensions{i}]);
    temp2 = cell(length({temp.name}),1);
    temp2(:) = {1};
    files = [files;[{temp.name}',temp2]];
end

% Check for subfolders
temp = dir(workingDir);
subDir = {temp([temp.isdir]).name};
% Append trailing / if needed
for i = 1:length(subDir)
    if subDir{i}(end) ~= '/'
        subDir{i} = strcat(subDir{i},'/');
    end
end

% Get rid of hidden folders 
subDir = subDir(~cellfun(@(x) strcmp(x(1),'.'),subDir));

% Add subfolder images
for i = 1:length(subDir)
    for j = 1:size(extensions,2)
        temp = dir([workingDir,subDir{i},extensions{j}]);
        temp2 = cell(length({temp.name}),1);
        temp2(:) = {i+1};
        files = [files;[{temp.name}',temp2]];
    end
end

% Add '' to subdir list for parent directory
subDir = [{''},subDir];

% Error if no files found
if size(files,1) == 0
    error(sprintf(['No image files found. \nAccepted formats: ',strjoin(extensions,', ')]));
end

% Scramble list order
files = files(randperm(length(files)),:);

%% Image classification GUI

numCat = length(catList);

% Create initial image figure
f1 = figure('position',[200 278 560 420],'Name','Image','NumberTitle','off');
currImageIndex = 1;
% Create progress string
imcount = uicontrol('Style', 'text', 'String', ['Image 1 / ',num2str(length(files))],...
    'FontSize', 16,'HorizontalAlign','center','Units','normalized',...
    'FontWeight','Bold','Position', [.3 .95 .4 .05]);
% Create credits string
uicontrol('Style', 'text', 'String','Georgia Squyres, Harvard University, 2016',...
    'FontSize', 10,'HorizontalAlign','center','Units','normalized','Position', [.3 .02 .4 .05]);

% Create classifier panel
f2 = figure('position',[772 397 180 300],'Name','Classifier','NumberTitle','off');
% Buttons for image classification categories
% Create button position lists
xpos = [];
if numCat < 9
    xpos(1:numCat) = .3;
    ypos = (.95-.85/(numCat)*(1:numCat)+.85/(numCat*2));
else
    xpos(1:ceil(numCat/2)) = .1;
    xpos(ceil(numCat/2)+1:numCat) = .5;
    ypos = (.95-.85/ceil(numCat/2)*(1:ceil(numCat/2))+.85/(ceil(numCat/2)*2));
    ypos = [ypos, ypos];
end
for i = 1:numCat  
    uicontrol('Style', 'pushbutton', 'String', catList(i),'FontSize', 12,...
        'Units','normalized','Position', [xpos(i) ypos(i) .4 .1],'Callback', {@btnClassCB,i});
end
% Divider
annotation('line',[0 1],[0.15 0.15]);
% Skip button
uicontrol('Style', 'pushbutton', 'String', 'skip','FontSize', 12,...
    'Units','normalized','Position', [.05 .03 .4 .1],'Callback', {@btnClassCB,numCat+1});
% Back button
backBtn = uicontrol('Style', 'pushbutton', 'String', 'back','FontSize', 12,'Enable','off',...
    'Units','normalized','Position', [.55 .03 .4 .1],'Callback', @btnBackCB);

% Add skip button to the category list
catList = [catList,'skipped'];
numCat = length(catList);

% Draw first figure
imageDraw(currImageIndex)
uiwait(f1)

%% Callbacks

% Callback for the classification button presses, including 'skip'
function btnClassCB(~,~,input)
    % enter classification
    files{currImageIndex,3} = input;
    % move to next figure and draw
    currImageIndex = currImageIndex+1;
    imageDraw(currImageIndex)
end

% Back button callback
function btnBackCB(varargin)
    % step back to previous image
    currImageIndex = currImageIndex - 1;
    imageDraw(currImageIndex);
end

%% Image Drawer

function imageDraw(currImageIndex)
    if currImageIndex <= length(files)
        % set back button status
        if currImageIndex < 2
        	set(backBtn,'Enable','off');
        else
            set(backBtn,'Enable','on');
        end
        % load current image file
        figure(f1);
        currImage = imread([workingDir,subDir{files{currImageIndex,2}},files{currImageIndex,1}]);
        % draw unscaled image
        if unscaled
            if autoscaled
                subplot(1,2,1);
            end
            imshow(currImage);
            axis equal tight; set(gca,'XTick',[]); set(gca,'YTick',[]);
            if autoscaled
                title('original','FontSize',14);
            end
        end
        % draw contrast adjusted image
        if autoscaled
            if unscaled
                subplot(1,2,2);
                title('autoscaled','FontSize',14);
            end
            imagesc(currImage); colormap gray;
            axis equal tight; set(gca,'XTick',[]); set(gca,'YTick',[]);
            if unscaled
                title('autoscaled','FontSize',14);
            end
        end
        % progress box
        set(imcount,'String',['Image ',num2str(currImageIndex),' / ',...
            num2str(length(files))]);
    else
        close(f1); close(f2);
        imageResults;
    end
end

%% End the program

function imageResults
    
    % build output structure array
    results = struct();
    for ii = 1:length(subDir)
        filesInSub = files([files{:,2}] == ii,:);
        results(ii).directory = subDir{ii};
        for jj = 1:numCat
             temp3 = filesInSub([filesInSub{:,3}] == jj,1);
             if ~isempty(temp3)
                results(ii).(catList{jj}) = temp3;
             else
                results(ii).(catList{jj}) = [];
             end
        end
    end
    
    % remove directories with no images
    for ii = length(results):-1:1
        temp3 = 0;
        for jj = 1:numCat
            temp3 = temp3 + isempty(results(ii).(catList{jj}));
        end
        if temp3 == numCat
            results(ii) = [];
        end
    end
end

end
