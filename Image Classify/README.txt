README: Image Classify

WRITTEN BY: Georgia Squyres, Garner Lab, Harvard University
In MATLAB R2016a
Contact: squyres@g.harvard.edu 

PUBLISHED IN: PUBLISHED IN: Bisson-Filho et al., Science, 2017. doi: 10.1126/science.aak9973
If you use this code, please cite this work.


INCLUDED SCRIPTS AND FUNCTIONS: 

imageClassify.m
sampleScript.m


DESCRIPTION: 

This program is a user interface for sorting images into user-defined categories, while blinding the user to the identities of the images. This can be used for comparing images in a single data set, or across multiple data sets, without biasing the results. 

imageClassify.m contains the program itself. sampleScript.m is an example of a way to parse the results.


INSTRUCTIONS FOR USE:

Before you begin, organize the images you wish to classify. To classify images from a single data set, choose a directory containing the images you wish to analyze. To classify images from multiple data sets, put each set of images in its own directory, and then put all of those folders into a master directory. Choose this master directory when running the program.

To begin, run imageClassify.m; the program takes no inputs. The program takes images from user-specified directories and presents them for classification in a random order. While the program is running, look at each image, and then select the category to which it belongs. If you are unsure, you can use the “skip” button to skip an image. The category assigned to each image will be recorded. If you make a mistake, you can undo your previous classifications by pressing the “back” button. 

Once the classification is completed, the program returns the results of the classification. In particular, it returns a structure containing fields for each category. Each field in the structure contains a list of file names assigned to that category (including those that were skipped). In multiple dataset mode, the structure will have multiple elements, with each element corresponding to a dataset (i.e. a subdirectory within the master directory). 

The structure returned by MATLAB can be parsed for statistics or graphing: sampleScript.m contains examples of how to manipulate the output structure. 

The number and the names of the categories can be selected by the user. To do this, edit the catList parameter in the “user-specified parameters” section of imageClassify.m. catList should be specified as a cell array of strings, with each string corresponding to a category. The program can gracefully handle up to 18 categories. The category names must start with a letter and cannot contain spaces or special characters. For example, if you wish to classify images into groups A, B, and C, you can specify catList as:
catList = {‘groupA’,’groupB’,’groupC’}. 

The image in the display panel can be shown to the user with no intensity scaling, with min/max intensity autoscaling, or with both intensity scaling modes side by side. This preference is set in the “user-specified parameters” section of the code. To see the image unscaled, set unscaled = true; to see it min/max autoscaled, set autoscaled = true. If both are set to true, both display modes will be shown side by side.

By default, image types accepted are: TIFF, JPEG, PNG. If you wish to change the image types accepted by the program, you can modify the “extensions” variable in the “user-specified parameters” section of the code. 
