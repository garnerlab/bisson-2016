% sampleScript.m
% 
% This script provides an example of how to parse the results structure
% generated by imageClassify.m and plot the results in a stacked bar graph.
%
% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

% run imageClassify
results = imageClassify;

% fetch category names
fnames = fieldnames(results);

% get the number of images in each category for each directory
resultsParsed = [];
for i = 1:length(results)
    for j = 2:length(fnames)-1 % ignoring directory name and skipped images
        resultsParsed(i,j-1) = size(results((i)).(fnames{j}),1);
    end
end

% normalize to number of images
for i = 1:size(resultsParsed,1)
    resultsParsed(i,:) = resultsParsed(i,:)./sum(resultsParsed(i,:));
end

% plot stacked bar graph of results
bar(resultsParsed,'stacked','LineWidth',2)

% label x axis
set(gca,'XTick',1:size(results,2));
set(gca,'XTickLabel',extractfield(results,'directory'));

% create legend
legend(fnames(2:end-1))
% expand axes for legend
xlim([0.5 size(results,2)*1.5]);

% plot aesthetics
set(gca,'LineWidth',2);
set(gca,'FontSize',18);
set(gca,'TickDir','out');
set(gca,'box','off');
temp = ylabel('Normalized frequency');
set(temp,'FontSize',24);