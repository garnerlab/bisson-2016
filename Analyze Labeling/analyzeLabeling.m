% analyzeLabeling.m
% Labeling Intensity Analysis 
% 
% Inputs: mode
%
% This program analyzes the amount and spatial distribution of signal in an
% imaging data set. Begin by selecting the directory containing the images
% you wish to analyze. Warning: your images should all have the same bit depth.
% 
% The analysis can be done in three modes: 
% Mode = 1 (total intensity), adds the intensity value of every pixel in 
% each image.
% Mode = 2 (total area) counts the number of pixels in each image above a
% threshold. 
% Mode = 3 (feature area) binarizes each image using a threshold, and then
% measures the average size of a binarized area
%
% Choose the mode you wish to use by inputting the number 1, 2, or 3
% Example: analyzeLabeling(1) will analyze the images in the chosen
% directory in the total intensity mode.
%
% Mode 2 and mode 3 require a choice of threshold, and you will be prompted
% to choose a threshold in the command window. 
% Your choice of threshold can either be a single value or a vector of
% thresholds to try.
%
% If you choose mode 1, or modes 2-3 with a single threshold, the program
% will plot a histogram of the results and return a vector of the result
% for each image.
%
% If you choose modes 2-3 with a threshold vector, the program will plot
% the mean and standard deviation result for each threshold in the vector.
% This can be helpful for initial threshold selection, and for determining 
% how your choice of threshold may affect your analysis. It will return a 
% cell array of results, with each element of the cell array containing the
% results for one threshold value.
%
% By default, image types accepted are: TIFF, JPEG, PNG
%
% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

function allProcessed = analyzeLabeling(mode)

%% user-specified parameters

% extensions: list of image file types to accept
extensions = {'*.tiff','*.tif','*.jpg','*.png'};

% === end of user-specified parameters ===

%% selecting images

% check key parameter errors
% check for missing input
try
    temp = mode;
catch
    error('Not enough input arguments: please select a mode.');
end
% check for missing extensions list
if isempty(extensions)
    error(sprintf('You have not enabled any image file extensions. \nPlease add extensions before proceeding.'));
% check for invalid mode
elseif sum(mode==1:3)==0
    error('Invalid mode. Please choose mode 1, 2, or 3.');
end

% Prompt to choose directory
workingDir=uigetdir();
% Exit if cancel button is pressed
if workingDir == 0
    error ('Analysis cancelled.');
end
% Append trailing / on directory name if needed
if workingDir(end) ~= '/'
    workingDir = [workingDir,'/'];
end
    
% Get list of image files
files = {};
for i = 1:size(extensions,2)
    temp = dir([workingDir,extensions{i}]);
    files = [files,{temp.name}];
end

% Error if no files found
if size(files,1) == 0
    error(sprintf(['No image files found. \nAccepted formats: ',strjoin(extensions,', ')]));
end

%% image processing

% Prompt for threshold choice: only needed for modes 2 and 3
if mode~=1
    temp = imfinfo([workingDir,files{i}]);
    goodInput = false;
    while ~goodInput
        try
            thresh = input(['Choose a threshold (0-',num2str(2^temp.BitDepth-1),'): ']);
        catch
        end
        % Check input, repeat prompt if bad
        if isnumeric(thresh)
            goodInput = true;
        end
    end
else
    % Set dummy thresh value for mode 1
    thresh = -1;
end

% Min max sort thresh:
thresh = sort(thresh);

allProcessed = cell(length(thresh),1);

% Loop through image files
for i = 1:length(files)
    
    % Load image
    currImage = imread([workingDir,files{i}]);
    
    % Loop through threshold vector
    for j = 1:length(thresh)
        
        % Mode 1: total intensity 
        if mode == 1
            allProcessed{j}(end+1,1) = sum(sum(currImage));

        % Mode 2: total area
        elseif mode == 2
            bw = currImage > thresh(j);
            allProcessed{j}(end+1,1) = sum(sum(bw));

        % Mode 3: feature area
        elseif mode == 3
            bw = currImage > thresh(j);
            cc = bwconncomp(bw, 4);
            if cc.NumObjects > 0
                areas = regionprops(bw,'Area');
                areas = extractfield(areas,'Area');
                areas = areas(areas>2); % Remove 1-2 pixel areas
                allProcessed{j} = [allProcessed{j},areas];
            end 
        end
    end
    
end

%% plotting

% Set axis label by mode
if mode == 1
    axlab = 'Total intensity (AU)';
elseif mode == 2
    axlab = 'Total area labeled (pixels)';
elseif mode == 3
    axlab = 'Feature area (pixels)';
end

figure; hold on;
% For single threshold values, plot a histogram
if length(thresh) == 1
    % Return an array instead of a 1-element cell array
    allProcessed = allProcessed{1};
    % Plot histogram
    histogram(allProcessed)
    % Labels
    xlabel(axlab);
    ylabel('Counts');
% For threshold vector, plot means and standard deviations
else
    % Plot standard deviations
    % Create standard deviation x vector by concatenating threshold vector
    try 
        temp = [thresh,thresh(end:-1:1)]; 
    catch
        temp = [thresh;thresh(end:-1:1)]; % in case thresh is a vertical vector
    end
    % Create shaded standard deviation region
    means = cellfun(@mean,allProcessed);
    stds = cellfun(@std,allProcessed);
    stdvector = [means+stds;means(end:-1:1)-stds(end:-1:1)];
    fill(temp(~isnan(stdvector)),stdvector(~isnan(stdvector)),[0.8 0.8 0.8],'EdgeColor','none');
    % Plot means
    plot(thresh,means,'LineWidth',2);
    % Axes and labels
    xlabel('Threshold');
    ylabel(axlab);
    xlim([min(thresh),max(thresh)]);
    ylim([0,max(means)]);
end

% Plot aesthetics
set(gca,'LineWidth',2);set(gca,'FontSize',18);
set(gca,'TickDir','out');