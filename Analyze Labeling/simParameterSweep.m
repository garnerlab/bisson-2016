% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

function simParameterSweep

% Data directories
dirs = {[pwd,'/pc19 60/'];
    [pwd,'/zmut/'];
    [pwd,'/mciz/']};

allProcessed = cell(1,6);
    
% Range of thresholds to test
thresh = linspace(0,1,100).*255;

for j = 1:3

    % Check for subfolders
    temp = dir(dirs{j});
    subDir = {temp([temp.isdir]).name};
    % Add slashes
    for i = 1:length(subDir)
        if subDir{i}(end) ~= '/'
            subDir{i} = strcat(subDir{i},'/');
        end
    end

    % Get rid of hidden folders 
    subDir = subDir(3:end);

    files = cell(length(subDir),1);

    % Add subfolder image
    for i = 1:length(subDir)
        temp = dir([dirs{j},subDir{i},'*.tif']);
        files{i} = {temp.name};
    end

    if size(files,1) == 0
        error('No image files found.')
    end

    for i = 1:length(subDir)
        
        allProcessed{1,j*2+i-2} = zeros(length(files{i}),length(thresh));
        
        for k = 1:length(files{i})
            % Load image
            currImage = imread([dirs{j},subDir{i},files{i}{k}]);
            % Crop image to consistent size
            center = round([size(currImage,1)/2,size(currImage,2)/2]);
            currImage = currImage(center(1)-31:center(1)+32,center(2)-31:center(2)+32);
            
            % OPTION 1: LABELING ABOVE THRESH
            for n = 1:length(thresh)
                bw = currImage > thresh(n);
                allProcessed{1,j*2+i-2}(k,n) = sum(sum(bw));
            end
            %}
            
            %{
            % OPTION 2: SUM INTENSITY
            allProcessed{1,j*2+i-2}(end+1,1) = sum(sum(currImage));
            %}
            
            %{
            % OPTION 3: FEATURE SIZES
            for n = 1:length(thresh)
                bw = currImage > thresh(n);
                cc = bwconncomp(bw, 4);
                if cc.NumObjects > 0
                    areas = regionprops(bw,'Area');
                    areas = extractfield(areas,'Area');
                    bleh{1,j*2+i-2}{n}(end+1:end+length(areas)) = areas;
                end 
            end
            %}
            
            %{
            % OPTION 4: FEATURE NUMBER
            bw = currImage > thresh;
            cc = bwconncomp(bw, 4);
            if cc.NumObjects > 0
                allProcessed{1,j*2+i-2}(end+1,1) = cc.NumObjects;
            end
            %}
            
        end
    end
    
end

%%

avgs = cellfun(@mean,allProcessed,'UniformOutput',0);

figure
hold on
temp = line([0 255],[0 0]);
set(temp,'Color','k');set(temp,'LineWidth',2)
h = [];
for i = 1:2:5
    diffs = avgs{i+1} - avgs{i};
    h(end+1) = plot(thresh,diffs,'LineWidth',4);
end
%}

%{
figure
hold on
z = zeros(1,length(diffs));
cols = ones(1,length(diffs));
cols(diffs<=0) = 2;
temp = [0 0.8 0
    1 0 0];
colormap(temp);
q = surface([thresh;thresh],[diffs;diffs],[cols;cols],...
        'facecol','no','edgecol','interp','linew',4);
%}
    
temp = legend(h,'PC19','D213A','MciZ');
set(temp,'Box','off'); set(temp,'FontSize',24);

% Plot aesthetics
xlim([0 255]);
set(gca,'LineWidth',2);set(gca,'FontSize',20);
set(gca,'TickDir','out');set(gca,'box','off')
temp = ylabel('Mean labeled area vs WT');
set(gca,'YTickLabel',{});
set(temp,'FontSize',28);
temp = xlabel('Threshold');
set(temp,'FontSize',28);