% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

function allProcessed = simCompare

% Data directories
dirs = {[pwd,'/pc19 60/'];
    [pwd,'/zmut/'];
    [pwd,'/mciZ/'];};

allProcessed = cell(1,6);

thresh = 20;

for j = 1:3

    % Check for subfolders
    temp = dir(dirs{j});
    subDir = {temp([temp.isdir]).name};
    % Add slashes
    for i = 1:length(subDir)
        if subDir{i}(end) ~= '/'
            subDir{i} = strcat(subDir{i},'/');
        end
    end

    % Get rid of hidden folders 
    subDir = subDir(3:end);

    files = cell(length(subDir),1);

    % Add subfolder image
    for i = 1:length(subDir)
        temp = dir([dirs{j},subDir{i},'*.tif']);
        files{i} = {temp.name};
    end

    if size(files,1) == 0
        error('No image files found.')
    end

    for i = 1:2
        for k = 1:length(files{i})
            % Read image
            currImage = imread([dirs{j},subDir{i},files{i}{k}]);
            % Crop image
            center = round([size(currImage,1)/2,size(currImage,2)/2]);
            currImage = currImage(center(1)-31:center(1)+32,center(2)-31:center(2)+32);
            
            %
            % OPTION 1: LABELING ABOVE THRESH
            %thresh = .2*max(max(currImage));
            bw = currImage > thresh;
            allProcessed{1,j*2+i-2}(end+1,1) = sum(sum(bw));
            %}        
            
            %{
            % OPTION 2: SUM INTENSITY
            allProcessed{1,j*2+i-2}(end+1,1) = sum(sum(currImage));
            %}
            
            %{
            % OPTION 3: FEATURE SIZES
            bw = currImage > thresh;
            cc = bwconncomp(bw, 4);
            if cc.NumObjects > 0
                areas = regionprops(bw,'Area');
                areas = extractfield(areas,'Area');
                areas(areas>2);
                allProcessed{1,j*2+i-2}(end+1:end+length(areas),1) = areas;
            end 
            %}
            
            %{
            % OPTION 4: FEATURE NUMBER
            bw = currImage > thresh;
            cc = bwconncomp(bw, 4);
            if cc.NumObjects > 0
                allProcessed{1,j*2+i-2}(end+1,1) = cc.NumObjects;
            end
            %}
            
        end
    end
    
end

colors = [0.5 0.5 0.5; 0.8 0.8 0.8];
colors = repmat(colors,3,1);
xpos = [1 1.8 3 3.8 5 5.8];

figure
[h,leg] = violin(allProcessed,'medc','','facecolor',colors,'x',xpos);
delete(leg)

ylim([0 1300]);
xlim([xpos(1)-0.6,xpos(end)+0.6]);

for i = 1:2:5
    [~,pval] = ttest2(allProcessed{i},allProcessed{i+1});
    ytemp = max([h(i).Vertices(:,2);h(i+1).Vertices(:,2)])+100;
    temp = line([xpos(i)-0.2 xpos(i+1)+0.2],[ytemp ytemp]);
    set(temp,'LineWidth',2); set(temp,'Color','k');
    if pval <= 0.0001
        tstr = '****';fs = 30;temp = 0;
    elseif pval <= 0.001
        tstr = '***';fs = 30;temp = 0;
    elseif pval <= 0.01
        tstr = '**';fs = 30;temp = 0;
    elseif pval <= 0.05
        tstr = '*';fs = 30;temp = 0;
    else
        tstr = 'ns';fs = 20;temp = 50;
    end
	text(mean([xpos(i),xpos(i+1)]),ytemp+temp,tstr,'FontSize',fs,'HorizontalAlignment','center');
end

% Plot aesthetics
set(gca,'LineWidth',2);set(gca,'FontSize',22);
set(gca,'TickDir','out');
set(gca,'XTick',xpos);
set(gca,'XTickLabel',{'-','+','-','+','-','+'});
temp = ylabel('Labeled area (pixels)');
%set(gca,'YTickLabel',{});
set(temp,'FontSize',30);

temp = line([2.4 2.4],[0 100000]);
set(temp,'Color','k');set(temp,'LineWidth',2)
temp = line([4.4 4.4],[0 100000]);
set(temp,'Color','k');set(temp,'LineWidth',2)
%}