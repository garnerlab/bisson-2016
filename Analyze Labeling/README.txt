README: Analyze Labeling Intensity

WRITTEN BY: Georgia Squyres, Garner Lab, Harvard University
In MATLAB R2016a
Contact: squyres@g.harvard.edu 

PUBLISHED IN: PUBLISHED IN: Bisson-Filho et al., Science, 2017. doi: 10.1126/science.aak9973
If you use this code, please cite this work.


INCLUDED SCRIPTS AND FUNCTIONS: 

analyzeLabeling.m
simCompare.m
simParameterSweep.m


DESCRIPTION: 

This program analyzes the amount and/or spatial distribution of signal in a data set. It can be used to quantify the amount of fluorescent label in a sample, bearing in mind some important considerations for use. 

The program can be run in one of three modes. Mode 1 (total intensity) adds the intensity value of every pixel in each image. Mode 2 (total area) counts the number of pixels in each image above a threshold. Mode 3 (feature area) binarizes each image using a threshold, and then measures the average size of a binarized area. 

If you wish to use this code in your own research, you will only need to download analyzeLabeling.m, and follow the instructions below.

The other functions provided (simCompare and simParameterSweep) are intended to document how we used this code in our own research, and are not meant for general use. These functions were used to analyze septal FDAA labeling under various experimental conditions that altered the treadmilling velocity of FtsZ. The results are found in figure 4B and S6B of Bisson-Filho et al., 2017. 

To run simCompare, you will also need the violin.m function to generate violin plots:

https://www.mathworks.com/matlabcentral/fileexchange/45134-violin-plot 


INSTRUCTIONS FOR USE:

Before you begin, put all the images you wish to analyze in a single directory. For accurate analysis, the images must be acquired and processed identically and have the same pixel dimensions. 

The program can be called as analyzeLabeling, and takes one input: your choice of mode. Example: analyzeLabeling(1) will analyze the images in the chosen directory in the total intensity mode.

Once you begin, the program will prompt you for a directory. You should select the directory containing the images you wish to analyze.

Mode 2 and mode 3 require a choice of threshold, so if you run the program in these modes, you will be prompted to choose a threshold in the command window. The prompt will suggest a range of thresholds to choose from based on the bit depth of your image. Your choice of threshold can either be a single value or a vector of thresholds. 

If you choose mode 1, or modes 2-3 with a single threshold, the program will plot a histogram of the results and return a vector containing the result of the analysis for each image in the chosen directory. 

If you choose modes 2-3 with a threshold vector, the program will plot the mean (red line) and standard deviation (gray shaded area) of the results for each threshold. This can be helpful for initial threshold selection, and for determining how your choice of threshold may affect your analysis. It will return a cell array of results, with each element of the cell array containing the results for one threshold value. 

In mode 3, it can be useful to exclude labeled regions with very small areas, since they are very likely to be artifactual. By default, the program excludes labeled regions of 1-2 pixels.

By default, image types accepted are: TIFF, JPEG, PNG. If you wish to change the image types accepted by the program, you can modify the “extensions” variable in the “user-specified parameters” section of the code. 
