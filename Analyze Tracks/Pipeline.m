% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

subdirs = {'FtsZ/','FtsA/','Pbp2B/'};

for i = 1:length(subdirs)
    files = dir([subdirs{i},'*.xml']);
    files = extractfield(files,'name');
    for j = 1:length(files)
        temp = strsplit(files{j},'.');
        if ~exist([subdirs{i},temp{1},'.mat'])
            tracks = importTrackMateTracks([subdirs{i},files{j}]);
            temp = strsplit(files{j},'.');
            save([subdirs{i},temp{1},'.mat'],'tracks');
        end
    end
end

%%

subdirs = {'FtsZ/','FtsA/','Pbp2B/'};

velocity = cell(1,length(subdirs));
alpha = cell(1,length(subdirs));

for i = 1:length(subdirs)
    
    files = dir([subdirs{i},'*.mat']);
    files = extractfield(files,'name');
    
    for j = 1:length(files)
        
        tracks = open([subdirs{i},files{j}]);
        tracks = tracks.tracks;
        totals(1,j) = length(tracks);
        
        filtered = tracks(cellfun(@length,tracks)>4);
        filtered = filtered(cellfun(@length,filtered)<26);
        filtered = filtered(cellfun(@(x) sum((x(end,2:3)-x(1,2:3)).^2,2),filtered)>0.05);
        
        if length(filtered) > 0
            statTr = trackVelocity(filtered);
            if length(statTr) > 0
                velocity{i} = [velocity{i},extractfield(statTr,'v')];
                alpha{i} = [alpha{i},extractfield(statTr,'alpha')];
            end
        end
    end
end

%%

figure; hold on
for i = 1:length(subdirs)
    [a,b] = hist(velocity{i}.*1000,linspace(0,60,10));
    plot(b,a./sum(a),'LineWidth',3);
end

temp = legend('FtsZ','FtsA','Pbp2B');
temp.Box = 'off';

% Plot aesthetics
set(gca,'LineWidth',2);set(gca,'FontSize',22);
set(gca,'TickDir','out');
temp = xlabel('Velocity (nm/s)');
set(temp,'FontSize',30);
temp = ylabel('Normalized frequency');
set(temp,'FontSize',30);