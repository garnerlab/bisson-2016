% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

function trackShow(tracks)

%% Initialization

% Choose the file
[Img,pixSize] = loadBFImg;

% Count number of frames
NumberImages = size(Img,3);

% Viewing mode toggles
showTracks = true;
showStatic = false;

% Generate an index of tracks to show at each frame
% For full description, see function below
[frameIndex,staticIndex] = getFrameIndex;

% Create initial figure
f = figure;
colormap gray
imagesc(Img(:,:,1));
axis equal
axis1 = gca;
isPlaying = false;

% axis limits for zoom out
fullSize = axis;

%% Create GUI elements: image window

% Create slider for frame control
if NumberImages > 1
    sldFrame = uicontrol('Style', 'slider','Min',1,'Max',NumberImages,'Value',1,'SliderStep',[1/NumberImages,10/NumberImages], ...
        'Units','normalized','Position', [0.2 0.05 .7 .01], 'Callback',@sldFrame_Callback);
    % Listener for dynamic update on click and drag
    addlistener(sldFrame,'ContinuousValueChange',@sldFrame_Callback);
end

% Create play button
if NumberImages > 1
    playBtn = uicontrol('Style', 'pushbutton', 'String', 'Play',...
        'Units','normalized','Position', [0.125 .025 .075 .05],'Callback', @playBtn_Callback);  
end

% Create zoom reset pushbutton
btnZoom = uicontrol('Style', 'pushbutton', 'String', 'Reset Zoom',...
    'Units','normalized','Position', [0.1 0.93 .15 .05],'Callback', @btnZoom_Callback);  

% Create frame selector text box
editFrame = uicontrol('Style', 'edit', 'String', '1',...
    'Units','normalized','Position', [0.9 0.93 .05 .05],'Callback', @editFrame_Callback);
% Frame box label
annotation('textbox', [.82, .98, 0, 0], 'string', 'Frame:','FontWeight','bold')

%% Create GUI elements: parameter window 

% Create panel
parFig = figure('position',[900 500 180 200],'Name','Track Parameters','NumberTitle','off','Resize','off','MenuBar','none','Toolbar','none');
% Show tracks toggle button
btnTracks = uicontrol('Style', 'checkbox', 'String', 'Show tracks','FontSize',12,'Value',1,...
    'Units','normalized','Position', [.1 .85 .8 .1],'Callback', @btnTracks_Callback);
% Static track view toggle box
btnStatic = uicontrol('Style', 'checkbox', 'String', 'Static tracks','FontSize',12,'Value',0,...
    'Units','normalized','Position', [.1 .1 .8 .1],'Callback', @btnStatic_Callback);

%% GUI callbacks: image window

% Slider callback: updates displayed image on slide
function sldFrame_Callback(varargin)
% show current image
slice = round(get(sldFrame,'Value'));
figure(f);
limits = axis;
imagesc(Img(:,:,slice),'Parent',axis1);
axis(limits); % preserves zoom
hold on
% show tracks
if showTracks
    % static tracks
    if showStatic
        for i = 1:length(staticIndex)
            currTrack = tracks(staticIndex(i)).track; % current coordinates
            plot(axis1,currTrack(:,2)./pixSize,currTrack(:,3)./pixSize,'r','LineWidth',2);
        end
    % dynamic tracks
    else
        for i = 1:size(frameIndex{slice},1)
            currParticle = frameIndex{slice}(i,1);
            currTrack = tracks(currParticle).track;
            currLength = frameIndex{slice}(i,2);
            plot(axis1,currTrack(1:currLength,2)./pixSize,currTrack(1:currLength,3)./pixSize,'r',...
                'LineWidth',2);
            %plot(axis1,currTrack(currLength,1),currTrack(currLength,2),'or');
        end
    end
end
hold off
set(editFrame,'String',num2str(slice)); % update frame number
end

% Button callback: resets zoom
function btnZoom_Callback(~,~)
axis(fullSize);
end

% Edit frame callback: jumps to selected frame
function editFrame_Callback(editFrame,~)
% parse user input
input1 = get(editFrame,'String');
newSlice = round(str2num(input1));
% check input
if isempty(newSlice)
    % reset slice position for incorrect input format
    slice = round(get(sldFrame,'Value'));
    set(editFrame,'String',num2str(slice));
else
    % trim large and small values
    if newSlice < 1
        newSlice = 1;
    elseif newSlice > NumberImages
        newSlice = NumberImages;
    end
    set(sldFrame,'Value',newSlice); % update slider postion
    sldFrame_Callback % display new image
    set(editFrame,'String',newSlice); % update frame number
end
end

% Play button callback
function playBtn_Callback(playBtn,~)
if ~isPlaying
    isPlaying = true;
    set(playBtn,'String','Pause')
    startPos = round(get(sldFrame,'Value'));
    for frame = startPos:NumberImages
        if isPlaying
            set(sldFrame,'Value',frame);
            sldFrame_Callback
            pause(0.1) % playback speed
        end
    end
    isPlaying = false;
    set(playBtn,'String','Play');
else
    isPlaying = false;
    set(playBtn,'String','Play')
end
end

%% GUI callbacks: parameter window

% Track button callback: toggles track visibility
function btnTracks_Callback(btnTracks,~)
    showTracks = get(btnTracks,'Value');
    % rationally toggle GUI elements
    if ~showTracks
        set(btnStatic,'Enable','off');
    else
        set(btnStatic,'Enable','on');
    end
    sldFrame_Callback % display new image
    figure(parFig)
end

% Static button callback: toggles static tracks
function btnStatic_Callback(btnStatic,~)
    showStatic = get(btnStatic,'Value');
    sldFrame_Callback % display new image
    figure(parFig)
end

%% Frame index generator

function [frameIndex,staticIndex] = getFrameIndex
% Creates movie frame index, which includes two parameters:
% 1. Track ID for each particle (i)
% 2. Which point in the track length you are at for each particle (j)
% Also creates static track list
frameIndex = cell(NumberImages,1);
staticIndex = [];
for i = 1:size(tracks,2)
    currTrack = tracks(i).track; % current coordinates
    if tracks(i).v>.01
        % Add track to frame index
        for j = 1:size(currTrack,1)
            frameIndex{currTrack(j,1)+1}(end+1,:)=[i j];
            % fix for skipped frames
            if j > 1 && (currTrack(j,1)-currTrack(j-1,1)) > 1
                % fill in time points with previous frame data
                for k = 1:(currTrack(j,1)-currTrack(j-1,1)-1)
                    frameIndex{currTrack(j-1,1)+k}(end+1,:)=[i j-1];
                end
            end     
        end
        % Add track to static track list
        staticIndex(end+1) = i;
    end
    end
end

end