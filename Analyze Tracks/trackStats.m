% trackStats.m
%
% Used to compute track statistic information, called by analyzeTracks.m
% This program calculates information about a particle track given its XY
% coordinates. 
% 
% Inputs: tracks (cell array)
% Outputs: statTr (structure)
% 
% Input tracks should be given as a cell array.
% Each cell contains a track, stored as an array
% Track array format: frame, x position, y position, z position
%
% This program is only compatible with 2D tracks. In this implementation,
% tracks must be in continuous frames (frame skipping is not allowed).
%
% Fields of the output structure are as follows:
% * track: column 1: frame, column 2: x position, column 3: y position,
% column 4: z position (unused)
% * msd: column 1: time step, column 2: mean squared displacement, column 3:
% standard deviation of squared displacement
% * alpha: alpha coefficient for velocity fit
% * rsquare: goodness-of-fit for log(MSD) vs log(dt) linear fit
% * v: particle velocity
% * D: particle diffusion coefficient
% * vfit: RESNORM of velocity fitting (see lsqcurvefit for details) 
%
% Track statistic options can be selected in the "user specified
% parameters" section.
%
% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017


function statTr = trackStats(tracks)
%% user specified parameters

% what fraction of possible time steps for a track will be used to compute MSDs
% set to a value between 0 and 1
% since n decreases at long time steps, I recommend using 0.8 or lower.
dtcutoff = 0.8;

% R-square requirement for log-log MSD fit
gOFf = 0.9; 

% === end of user specified parameters ===

%% compute MSDs

% create output structure
statTr = struct('track',{},'msd',{},'alpha',{},'rsquare',{},'v',{},'vfit',{});
% create MSD fit equation
fitfcn = @(x,t)(4*x(2)*t+(x(1)*t).^2);

% compute MSDs for each track
for i = 1:length(tracks)
    % MSD holding array
    msd = zeros(round(length(tracks{i})*dtcutoff),3);
    % Displacement holding array
    displ = cell(round(length(tracks{i})*dtcutoff),1);
    % Compute displacements
    for j = 1:round(length(tracks{i})*dtcutoff)
        endpos = tracks{i}(j+1:end,2:3);
        startpos = tracks{i}(1:end-j,2:3);
        displ{j} = sum((endpos-startpos).^2,2);
    end
    % Compute MSDs
    for j = 1:length(displ)
        msd(j,1) = j; % dt
        msd(j,2) = mean(displ{j}); % average
        msd(j,3) = std(displ{j}); % std
    end
    % compute log-log linear fit
    msdFit = regstats(log(msd(:,2)),log(msd(:,1))','linear',{'beta','rsquare'});
    % filter goodness-of-fit
    if msdFit.rsquare > gOFf
        % Fit to: 4D(t)+(Vt)2
        [~,v,vFit] = evalc('lsqcurvefit(fitfcn,[0,0],msd(:,1),msd(:,2),[0 0],[Inf Inf])');
        % Add to structure
        statTr(end+1).track = tracks{i};
        statTr(end).msd = msd;
        statTr(end).alpha = msdFit.beta(2);
        statTr(end).rsquare = msdFit.rsquare;
        statTr(end).v = v(1);
        statTr(end).D = v(2);
        statTr(end).vfit = vFit;
    end
end