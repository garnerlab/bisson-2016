% This program opens a Bio-Formats image and returns it as an array for 
% further processing. 
%
% Input: none (image is chosen through user prompt)
% Output: image matrix
%
% Dependencies: Bio-Formats MATLAB Toolbox
% You can download the Bio-Formats Toolbox from downloads.openmicroscopy.org
% (be sure to choose MATLAB Toolbox)
%
% Georgia Squyres, Garner Lab, Harvard University
% Written in MATLAB R2015a, August 2016
% Contact: squyres@g.harvard.edu

function [Img,pixelSize] = loadBFImg

% Add Bio-Formats to path
addpath(genpath(pwd))

% Load Bio-Formats 
autoloadBioFormats = 1;
try
    status = bfCheckJavaPath(autoloadBioFormats);
    assert(status, ['Missing Bio-Formats library. Please add Bio-Formats to the '...
        'MATLAB path. (Type ''help getCoords'' for details.)']);
catch
    error(['Missing Bio-Formats Toolbox. Please add the Bio-Formats MATLAB toolbox to the '...
        'MATLAB path. (Type ''help getCoords'' for details.)'])
end

% Get image file handle
extensions = bfGetFileExtensions;
extensions = extensions{1}; 
[fileName,filePath]=uigetfile(extensions,'Select a file');
if sum(fileName == 0) ~= 0 && sum(filePath == 0) ~= 0
    error('Processing cancelled. Goodbye!');
end

% Open image with Bio-Formats
Img = bfopen([filePath,fileName]);
pixelSize = double(Img{4}.getPixelsPhysicalSizeX(0).value(ome.units.UNITS.MICROM));
Img = cat(3,Img{1}{:,1});