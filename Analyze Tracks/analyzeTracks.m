% analyzeTracks.m
% Measures velocity and other track information from particle tracking data
% generated by TrackMate
% 
% Inputs: none
% Outputs: trackInfo (structure)
% 
% Select a TrackMate file, and get information about the tracks in that
% file. The TrackMate file must be exported using the "Export tracks to XML
% file" option in TrackMate. If you have already converted the selected
% track to .mat format, you can select the .mat file instead to save time.
% This program is only compatible with 2D tracks, and is not compatible
% with tracks containing skipped frames.
%
% Fields of the output structure are as follows:
% * track: column 1: frame, column 2: x position, column 3: y position,
% column 4: z position
% * msd: column 1: time step, column 2: mean squared displacement, column 3:
% standard deviation of squared displacement
% * alpha: alpha coefficient for velocity fit
% * rsquare: goodness-of-fit for log(MSD) vs log(dt) linear fit
% * v: particle velocity
% * D: particle diffusion coefficient
% * vfit: RESNORM of velocity fitting (see lsqcurvefit for details) 
%
% Track filtering options can be selected in the "user specified
% parameters" section.
%
% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017


function trackInfo = analyzeTracks

%% user specified parameters

% set the minimum length cutoff for tracks 
% this must be set to at least 4 for successful velocity fitting
minlength = 4;

% set the maximum length cutoff for tracks 
% set maxlength = -1 for no cutoff
maxlength = -1;

% set the minimim displacement cutoff for tracks
% set mindisp = -1 for no cutoff
mindisp = -1;

% === end of user specified parameters ===

%% open trackmate track, convert to .mat file if necessary

% Prompt to choose track
[fileName,filePath]=uigetfile({'*.xml';'*.mat'});
% Exit if cancel button is pressed
if fileName == 0
    error ('Track analysis cancelled.');
end
if filePath(end) ~= '/'
    filePath = [filePath,'/'];
end

% Convert .xml to .mat file
fileType = strsplit(fileName,'.');
fileType = fileType{end};
if strcmp(fileType,'xml')
    temp = strsplit(fileName,'.');
    % Import tracks
    try
        tracks = importTrackMateTracks([filePath,fileName]);
    catch ME % check whether the user has importTrackMateTracks.m
        if strcmp(ME.identifier,'MATLAB:UndefinedFunction')
            error('Download importTrackMateTracks.m to continue (see README for details)')
        else
            rethrow(ME)
        end
    end
    % This is slow, so save once it's done
    save([filePath,temp{1},'.mat'],'tracks');
    fileName = [temp{1},'.mat'];
end

% open tracks file
tracks = open([filePath,fileName]);
tracks = tracks.tracks;
% tracks should be a cell array
% each track in the image is stored as an array
% array format: frame, x position, y position, z position

%% process tracks

% filter tracks
% minimum length:
tracks = tracks(cellfun(@(x) size(x,1),tracks)>minlength);
% maximum length:
if maxlength > -1
    tracks = tracks(cellfun(@(x) size(x,1),tracks)<maxlength);
end
% end-to-end displacement:
if mindisp > -1
    tracks = tracks(cellfun(@(x) sum((x(end,2:3)-x(1,2:3)).^2,2),tracks)>mindisp^2);
end

% compute track information
if ~isempty(tracks)
    trackInfo = trackStats(tracks);
else
    warning('No tracks passed filtering')
end
