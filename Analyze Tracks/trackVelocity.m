% Written in MATLAB R2016a by:
% Georgia Squyres, Garner Lab, Harvard University
% contact: squyres@g.harvard.edu
% 2016-2017

function statTr = trackVelocity(tracks)

% PARAMETERS
dtcutoff = 0.8; % What percent of possible time steps will be used to compute MSDs

% Compute MSD for each track
disp('Computing MSDs...');

statTr = struct('track',{},'msd',{},'alpha',{},'rsquare',{},'v',{},'vfit',{});
fitfcn = @(x,t)(4*x(2)*t+(x(1)*t).^2);

for i = 1:length(tracks)
    % MSD holding array
    msd = zeros(round(length(tracks{i})*dtcutoff),3);
    % Displacement holding array
    displ = cell(round(length(tracks{i})*dtcutoff),1);
    % Compute displacements
    for j = 1:round(length(tracks{i})*dtcutoff)
        endpos = tracks{i}(j+1:end,2:3);
        startpos = tracks{i}(1:end-j,2:3);
        displ{j} = sum((endpos-startpos).^2,2);
    end
    % Compute MSD
    for j = 1:length(displ)
        msd(j,1) = j; % dt
        msd(j,2) = mean(displ{j}); % average
        msd(j,3) = std(displ{j}); % std
    end
    % check log-log linear fit
    msdFit = regstats(log(msd(:,2)),log(msd(:,1))','linear',{'beta','rsquare'});
    if msdFit.rsquare > 0.95
    % Fit to: 4D(t)+(Vt)2
    [~,v,vFit] = evalc('lsqcurvefit(fitfcn,[0,0],msd(:,1),msd(:,2),[0 0],[Inf Inf])');
    %if vFit <= 0.00002 % optional filter on velocity fitting
        %{
        % plot (slow, for debugging only)
        plot(msd(:,1),msd(:,2));
        hold on
        plot(msd(:,1),4*v(2)*msd(:,1)+(v(1)*msd(:,1)).^2);
        title(num2str(vFit));
        pause(1)
        hold off
        %}
        % add to structure
        statTr(end+1).track = tracks{i};
        statTr(end).msd = msd;
        statTr(end).alpha = msdFit.beta(2);
        statTr(end).rsquare = msdFit.rsquare;
        statTr(end).v = v(1);
        statTr(end).vfit = vFit;
    end
end